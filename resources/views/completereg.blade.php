<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="keywords" content="nitc, fossmeet, NITC, CSEA, FOSSCell, fossmeet, fossmeet 2017, nit, fossmeet nit, nit calicut"/>
    <meta name="author" content="FOSSCell, NITCalicut"/>
    <meta property="og:title" content="FOSSMeet '17"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:url" content="http://fossmeet.in/2017/"/>
    <meta property="og:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta property="og:locale" content="en_IN"/>
    <meta property="og:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta property="fb:app_id" content="1063442297042156"/>
    <!-- Twitter Cards-->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@fossmeet"/>
    <meta name="twitter:creator" content="@fosscell"/>
    <meta name="twitter:title" content="FOSSMeet '17"/>
    <meta name="twitter:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="twitter:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta name="twitter:image:alt" content="FOSSMeet '17"/>
    <!-- Google Structured data markup-->
    <script type="application/ld+json">{"@context":"http://schema.org","@type":"Event","name":"FOSSMeet 2017","image":"http://fossmeet.in/2017/img/yash2696.png","description":"FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut.","startDate":"2017-03-10T15:00:00+05:30","endDate":"2017-03-12T18:00:00+05:30","url":"http://fossmeet.in/2017/","location":{"@type":"Place","name":"National Institute of Technology Calicut","address":"NITC CAMPUS PO, Kettangal Koduvally Road Kozhikode, Kerala - 673601"}}</script>
    <!-- the Title of the Page-->
    <title> FOSSMeet 17 | FOSSCell NITC </title>
    <link rel="stylesheet" href="css/vendor.css" type="text/css"/><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5shiv.js"></script>
<script type="text/javascript" src="js/respond.js"></script>
<![endif]-->
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    <!-- the MANIFEST file-->
    <link rel="manifest" href="manifest.json"/>
    <meta name="msapplication-config" content="browserconfig.xml"/>
    <!-- http://realfavicongenerator.net/-->
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/favicons/manifest.json">
    <link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#1f2021">
    <link rel="shortcut icon" href="/img/favicons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="FOSSMeet '17">
    <meta name="application-name" content="FOSSMeet '17">
    <meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#1f2021">
  </head>
  <body ng-app="app">
    <div class="container-fluid" ng-controller="PreferenceController">
      <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
          <h2 class="center">Complete the registration.</h2>
          <div style="min-height:300px;">
            <div ng-if="isDone" style="padding-top:100px;">
              <div class="row center-xs">
                <div class="col-xs">
                  <h4>Your registration is complete.</h4><a class="bbtn" href="./">Go Back To Home</a>
                </div>
              </div>
            </div>
            <div ng-if="!fetched" style="padding-top:150px;">
              <loader></loader>
            </div>
            <div ng-if="fetched&amp;&amp;!isDone">
              <form name="form">
                <div class="formrow">
                  <p>Choose the worksops you would like to attend:</p>
                  <div class="row middle-xs">
                    <div class="col-xs-12 col-md-4" ng-repeat="w in workshops">
                      <radio model="w.checked" value="true" label="@{{w.name}}"></radio>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs" ng-if="!isLoading">
                    <button type="submit" tabindex="0" ng-click="register()">Submit</button>
                  </div>
                  <div class="col-xs" ng-if="isLoading">
                    <loader></loader>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-12">
          <h2 class="center">Invite your Friends</h2>
          <div class="center"><a class="social-link" href="https://share.diasporafoundation.org/?url=http%3a%2f%2ffossmeet.in%2f&amp;title=I%20am%20going%20to%20%40nilenso%20presents%20%40fossmeet%20in%20association%20with%20%40gitlab%20powered%20by%20IEEE%20and%20%40instamojo!%20Join%20me!"><i class="fa fa-asterisk"></i></a><a class="social-link" href="https://www.facebook.com/sharer/sharer.php?u=http%3a%2f%2ffossmeet.in%2f"><i class="fa fa-facebook"></i></a><a class="social-link" href="https://twitter.com/intent/tweet?text=I%20am%20going%20to%20%40nilenso%20presents%20%40fossmeet%20in%20association%20with%20%40gitlab%20powered%20by%20IEEE%20and%20%40instamojo!%20Join%20me!"><i class="fa fa-twitter"></i></a><a class="social-link" href="https://plus.google.com/share?url=http%3a%2f%2ffossmeet.in%2f"><i class="fa fa-google-plus-square"></i></a><a class="social-link" href="whatsapp://send?text=I%20am%20going%20to%20%40nilenso%20presents%20%40fossmeet%20in%20association%20with%20%40gitlab%20powered%20by%20IEEE%20and%20%40instamojo!%20Join%20me!"><i class="fa fa-whatsapp"></i></a></div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="js/config.js"></script>
    <script type="text/javascript" src="./js/vendor.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
  </body>
</html>