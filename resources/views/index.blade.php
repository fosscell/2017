<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="keywords" content="nitc, fossmeet, NITC, CSEA, FOSSCell, fossmeet, fossmeet 2017, nit, fossmeet nit, nit calicut"/>
    <meta name="author" content="FOSSCell, NITCalicut"/>
    <meta property="og:title" content="FOSSMeet '17"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:url" content="http://fossmeet.in/2017/"/>
    <meta property="og:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta property="og:locale" content="en_IN"/>
    <meta property="og:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta property="fb:app_id" content="1063442297042156"/>
    <!-- Twitter Cards-->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@fossmeet"/>
    <meta name="twitter:creator" content="@fosscell"/>
    <meta name="twitter:title" content="FOSSMeet '17"/>
    <meta name="twitter:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="twitter:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta name="twitter:image:alt" content="FOSSMeet '17"/>
    <!-- Google Structured data markup-->
    <script type="application/ld+json">{"@context":"http://schema.org","@type":"Event","name":"FOSSMeet 2017","image":"http://fossmeet.in/2017/img/yash2696.png","description":"FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut.","startDate":"2017-03-10T15:00:00+05:30","endDate":"2017-03-12T18:00:00+05:30","url":"http://fossmeet.in/2017/","location":{"@type":"Place","name":"National Institute of Technology Calicut","address":"NITC CAMPUS PO, Kettangal Koduvally Road Kozhikode, Kerala - 673601"}}</script>
    <!-- the Title of the Page-->
    <title> FOSSMeet 17 | FOSSCell NITC </title>
    <link rel="stylesheet" href="css/vendor.css" type="text/css"/><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5shiv.js"></script>
<script type="text/javascript" src="js/respond.js"></script>
<![endif]-->
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    <!-- the MANIFEST file-->
    <link rel="manifest" href="manifest.json"/>
    <meta name="msapplication-config" content="browserconfig.xml"/>
    <!-- http://realfavicongenerator.net/-->
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/favicons/manifest.json">
    <link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#1f2021">
    <link rel="shortcut icon" href="/img/favicons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="FOSSMeet '17">
    <meta name="application-name" content="FOSSMeet '17">
    <meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#1f2021">
  </head>
  <body ng-app="app">
    <div id="wrapper" style="overflow:hidden;" ng-controller="NavController">
      <div class="sidebar" id="sidebar-wrapper" ng-class="{'open':isOpen}">
        <ul class="sidebar-nav">
          <li class="active sidebar-brand" style="position:relative;"><a data-toggle="tab" href="#home" ng-click="toggle()"><span class="pull-left">FOSSMeet '17</span><img src="img/logo.svg" width="20em" height="20em" style="text-align:right;"/></a><i class="navicon hide-md fa fa-close menutoggleicon" style="position:absolute;top:0px;right:12px;" ng-click="toggle()"></i></li>
          <li></li>
          <div style="height:2em;border-style: none none solid none;border-color: #FFF;"></div>
          <li><a data-toggle="tab" href="#home" ng-click="toggle()">Home</a></li>
          <li><a data-toggle="tab" href="#what" ng-click="toggle()">About the Event</a></li>
          <li><a data-toggle="tab" href="#speakers" ng-click="toggle()">Speakers</a></li>
          <li><a data-toggle="tab" href="#reg" ng-click="toggle()">Registration</a></li>
          <li><a data-toggle="tab" href="#sponsors" ng-click="toggle()">Sponsors</a></li>
          <li><a data-toggle="tab" href="#schedule" ng-click="toggle()">Schedule</a></li>
          <li><a data-toggle="tab" href="#coc" ng-click="toggle()">Code of Conduct</a></li>
          <li><a data-toggle="tab" href="#contact" ng-click="toggle()">Contact Us</a></li>
        </ul>
      </div>
      <div id="page-content-wrapper" style="overflow:hidden;">
        <div class="header hide-md"><i class="navicon fa fa-bars menutoggleicon" ng-click="toggle()"></i>
          <h4 style="display:inline-block;"><a href="./" style="color:white;text-decoration:none; font-size:1.5rem;padding-left:15px;">FOSSMeet '17</a></h4>
        </div>
        <section style="overflow:hidden;">
          <div class="tab-content" style="overflow-y:auto;">
            <div class="tab-pane fade in active" id="home">
              <div class="header"><img src="./img/logo.svg" style="width:100px;"/>
                <h1>FOSSMeet '17</h1>
                <p style="letter-spacing: 0.2em;">Think Free, Code Better</p>
                <h3>March 10 - 12, 2017</h3><a class="bbtn" data-toggle="tab" href="#reg" ng-click="toggle()" style="padding: 2px 20px;margin: 0;">Register</a>
              </div>
              <div id="home-inner">
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/09d1887610dd55fffeebdb18977d2df9_original.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/860dd99caeabf8b3ef87225688244e73.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/alex-tass-f-s-f-rebound.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/Apache_PoweredBy.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/Atul-Chitnis.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/BabyGnuAlpha.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/dt-logo-2015-test-files-r1-green-part.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/DuckDuckGo-Logo.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/e66a60789aa4622183362b8b19dba0bd.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/fedora.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/FOSS_logo.91151336_std.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/Gitlab_logo.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/git_logo.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/gnu-head.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/google.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/heart.svg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/hZda1.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/imENUbEK_400x400.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/KNOPPIX_logo.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/kodi.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/linus.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/linux-penguin-big_origpreview.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/mint8logo.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/Mozilla_Firefox_Logo.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/OBS.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/ohw.png"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/slow-firefox-ubuntu.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/stallman.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/ubuntu-logo.jpg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/ubuntu.svg"/></div>
                <div class="tile"><img class="imgtile img-fluid" src="./img/front/wordpress-logo-square.png"/></div>
              </div>
            </div>
            <div class="tab-pane fade" id="what">
              <div class="tabstuff pad_l">
                <div class="container-fluid">
                  <div class="row middle-xs">
                    <div class="col-xs-12 col-md-3 center"><img src="./img/icons/what.png" style="width:100px; border-radius: 50%;"/></div>
                    <div class="col-xs-12 col-md-9">
                      <h1>What is FOSSMeet?</h1>
                      <p class="justify">FOSSMeet is an annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut. Started with a vision to create a culture of innovation, evolution and open standards, the meet intends to support the FOSS community and the dissemination of FOSS ideology through hands-on sessions, discussions and lectures. Nothing better to get you introduced to the world of FOSS. </p>
                    </div>
                    <div class="row middle-xs">
                      <div class="col-xs-12 col-md-3 center"><img src="./img/icons/events.png" style="width:100px; border-radius: 50%;"/></div>
                      <div class="col-xs-12 col-md-9">
                        <h1>Pre-FOSSMeet Events</h1>
                        <p class="justify">This year, in order to introduce school children into the world of FOSS, FOSSCell NIT-C conducted an event called School FOSSMeet on February 18<sup>th</sup>. Students from over 10 schools around the campus had been invited for the event. More details regarding the event can be found <a href="https://school.fossmeet.in/">here</a>.</p>
                      </div>
                    </div>
                    <div class="row middle-xs">
                      <div class="col-xs-12 col-md-3 center"><img src="./img/icons/expect.png" style="width:100px; border-radius: 50%;"/></div>
                      <div class="col-xs-12 col-md-9">
                        <h1>What to expect?</h1>
                        <p class="justify">FOSSMeet will feature workshops, talks and discussions on a wide range of topics by established speakers in the FOSS arena. The hands-on sessions will orient you not just to develop using FOSS, but contribute to the same as well. Most importantly, FOSSMeet is a chance to meet like-minded people :-) </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="reg">
              <div class="container-fluid" ng-controller="RegController">
                <div class="row">
                  <div class="col-xs-12 col-md-8 col-md-offset-2" style="min-height:90vh;">
                    <div ng-if="!showForm" style="padding-top:50px;">
                      <h2>Registration Guidelines</h2>
                      <ul class="guidelines">
                        <li>One thing you should know, there are <b>limited</b> number of seats in some of the popular workshops. Act fast so that you dont miss out on any awesome workshop.</li>
                        <li>Online registration is the easiest way for you to act fast. We will provide you with a unique ID via email once you make the payment online, using which you will be able to tell us the list of your preferred workshops. FCFS rule applied here (First Come First Served).</li>
                        <li>If you wish to register online, then 5<sup>th</sup> March is the deadline.</li>
                        <li>In case you are not able to register online, you can still register for FOSSMeet '17 by coming here (NIT Calicut). College ID (for students)/Photo ID (for professionals), is mandatory.</li>
                        <li>A FOSSMeet '17 ID shall be provided at the registration desk. It shall be your ticket into the workshops you wish to attend (so please take care of the FOSSMeet ID!).</li>
                        <li>Even if you have registered online, you should report to the Registration Desk and provide your College ID (for students)/Photo ID (for professionals), so that we can confirm your participation and you can pick up your FOSSMeet '17 ID.</li>
                        <li>Accommodation shall be provided to all the participants who require it, on a First Come First Serve basis,  which might incur a nominal charge. This shall be taken care of at the Registration Desk.</li>
                        <li>Participants who wish to use their own laptops for hands-on sessions are expected to have the necessary softwares installed beforehand as specified in the schedule.</li>
                      </ul><a class="bbtn" ng-click="show()">I agree</a>
                    </div>
                    <form name="form" ng-if="showForm">
                      <h2 class="center">Register</h2>
                      <div class="formrow">
                        <input type="text" name="name" ng-model="in.name" required/>
                        <label for="name">Name</label>
                        <p class="error" ng-if="(form.name.$dirty||submitted)&&form.name.$invalid">Please enter your name.</p>
                      </div>
                      <div class="formrow">
                        <input type="email" name="email" ng-model="in.email" required/>
                        <label for="email">Email</label>
                        <p class="error" ng-if="(form.email.$dirty||submitted)&&form.email.$invalid">Please enter your email.</p>
                      </div>
                      <div class="formrow">
                        <input type="text" name="mobile" ng-model="in.mobile" required/>
                        <label for="mobile">Mobile</label>
                        <p class="error" ng-if="(form.mobile.$dirty||submitted)&&form.mobile.$invalid">Please enter your mobile number.</p>
                      </div>
                      <div class="formrow">
                        <p>Choose your registration type:</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.reg_type" value="TYPE_NITC" label="NITC Student ₹100"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.reg_type" value="TYPE_OTHER" label="Outside NITC Students ₹400"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.reg_type" value="TYPE_PROFESSIONAL" label="Professionals ₹800"></radio>
                          </div>
                        </div>
                        <p class="error" ng-if="submitted&&!in.reg_type">Please choose registration type.</p>
                        <div ng-show="in.reg_type=='TYPE_PROFESSIONAL'||in.reg_type=='TYPE_OTHER'">
                          <p>Name of your @{{ in.reg_type == 'TYPE_OTHER' ? 'college' : 'organization'}}:</p>
                          <div class="formrow">
                            <div class="row middle-xs">
                              <div class="col-xs">
                                <input type="text" name="organization_name" ng-model="in.organization_name"/>
                                <label for="organization_name">Name</label>
                                <p ng-if="(form.organization_name.$dirty||submitted)&&form.organization_name.$invalid">Please enter the name of your @{{ in.reg_type == 'TYPE_OTHER' ? 'college' : 'organization'}}.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="formrow">
                        <p>Gender:</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.gender" value="Male" label="Male"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.gender" value="Female" label="Female"></radio>
                          </div>
                        </div>
                        <p class="error" ng-if="submitted&&!in.gender">Please choose gender.</p>
                      </div>
                      <div class="formrow">
                        <p>Food preference:</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.food_preference" value="veg" label="Veg"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.food_preference" value="non veg" label="Non Veg"></radio>
                          </div>
                        </div>
                        <p class="error" ng-if="submitted&amp;&amp;!in.food_preference">Please choose food preference.</p>
                      </div>
                      <div class="formrow">
                        <p>Would you like to opt for a FOSSMeet '17 T-shirt?</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.need_tshirt" value="Yes" label="Yes"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.need_tshirt" value="No" label="No"></radio>
                          </div>
                        </div>
                      </div>
                      <div class="formrow" ng-if="in.need_tshirt=='Yes'">
                        <p>T-shirt size:</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.tshirt_size" value="S" label="S"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.tshirt_size" value="M" label="M"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.tshirt_size" value="L" label="L"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.tshirt_size" value="XL" label="XL"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.tshirt_size" value="XXL" label="XXL"></radio>
                          </div>
                        </div>
                        <p class="error" ng-if="submitted&amp;&amp;!in.tshirt_size">Please choose your tshirt size.</p>
                      </div>
                      <div class="formrow">
                        <p>Do you need to be provided accomodation?</p>
                        <div class="row middle-xs">
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.accomodation" value="Y" label="Yes"></radio>
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <radio model="in.accomodation" value="N" label="No"></radio>
                          </div>
                        </div>
                        <p class="error" ng-if="submitted&amp;&amp;!in.accomodation">Please choose select one of the options.</p>
                      </div>
                      <div class="row">
                        <div class="col-xs" ng-if="!isLoading">
                          <button type="submit" tabindex="0" ng-click="register()">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="speakers">
              <div class="tabstuff">
                <div class="container-fluid">
                  <div class="row center-xs">
                    <div class="col-xs">
                      <h1>Speakers</h1>
                    </div>
                  </div>
                  <div class="row center-xs">
                    <div class="col-xs"><a href="https://in.linkedin.com/in/abhasabhinav">
                        <div class="speaker-card"><img src="./img/speakers/abhas.png"/>
                          <h4>Abhas Abhinav</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="http://pramode.net/">
                        <div class="speaker-card"><img src="./img/speakers/pramode.png"/>
                          <h4>Pramode C E</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="https://in.linkedin.com/in/sasi-kumar-1a48688">
                        <div class="speaker-card"><img src="./img/speakers/sks.png"/>
                          <h4>Sasi Kumar</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="http://nibrahim.net.in/">
                        <div class="speaker-card"><img src="./img/speakers/noufal.png"/>
                          <h4>Noufal Ibrahim</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="https://in.linkedin.com/in/sarahmasud">
                        <div class="speaker-card"><img src="./img/speakers/sarah.png"/>
                          <h4>Sarah Masud</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="https://j15h.nu/">
                        <div class="speaker-card"><img src="./img/speakers/j15hnu.png"/>
                          <h4>Jishnu Mohan</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="https://twitter.com/kkmane">
                        <div class="speaker-card"><img src="./img/speakers/kkmane.png"/>
                          <h4>Krishnakant Mane</h4>
                        </div></a></div>
                    <div class="col-xs"><a href="https://fossmeet-nitc.talkfunnel.com/2017/">
                        <div class="speaker-card"><img src="./img/speakers/plus.png"/>
                          <h4>And Many More ...</h4>
                        </div></a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="sponsors">
              <div class="tabstuff">
                <div class="container-fluid">
                  <div class="row center-xs" style="padding-top:50px;">
                    <div class="col-xs-12 col-md-6"><a href="https://nilenso.com/">
                        <div class="row center-xs pad"><img class="large" src="./img/sponsors/nilenso.png"/></div></a>
                      <div class="row center-xs middle-xs pad" style="background-color: #FFF;border-radius: 1.5em;"><a href="https://about.gitlab.com/">
                          <div class="col-xs"><img class="small" src="./img/sponsors/gitlab.png"/></div></a><a href="http://icfoss.in/">
                          <div class="col-xs"><img class="small" src="./img/sponsors/icfoss.png"/></div></a>
                        <div style="height:2em;"></div><a href="http://ieee.nitc.ac.in/">
                          <div class="col-xs"><img class="small" src="./img/sponsors/ieee.png"/></div></a><a href="about:blank">
                          <div class="col-xs"><img class="small" src="./img/sponsors/instamojo.png"/></div></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="schedule" ng-controller="ScheduleController">
              <div class="tabstuff">
                <div class="container-fluid" style="min-height:90vh;">
                  <div class="row center-xs">
                    <div class="col-xs">
                      <h1>Schedule</h1>
                    </div>
                  </div>
                  <form>
                    <div class="row center-xs middle-xs">
                      <div class="col-xs-12 col-md-4">
                        <radio model="in.day" value="0" label="Day 1"></radio>
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <radio model="in.day" value="1" label="Day 2"></radio>
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <radio model="in.day" value="2" label="Day 3"></radio>
                      </div>
                    </div>
                  </form>
                  <div class="schedulestack">
                    <div class="head">
                      <div class="row center-xs">
                        <div class="col-md col-xs-12">
                          <h4>Software Systems Lab</h4>
                        </div>
                        <div class="col-md hide-xs">
                          <h4>Network Systems Lab </h4>
                        </div>
                        <div class="col-md hide-xs">
                          <h4>Chanakya Hall</h4>
                        </div>
                      </div>
                    </div>
                    <div class="body">
                      <div class="row">
                        <div class="col-xs-12 col-md" style="padding-top:20px;">
                          <div ng-repeat="s in schedulestack.v1">
                            <stack content="s"></stack>
                          </div>
                        </div>
                        <div class="col-xs-12 hide-md center">
                          <h4 ng-if="schedulestack.v2.length&gt;0">Network Systems Lab </h4>
                        </div>
                        <div class="col-xs-12 col-md" style="padding-top:20px;">
                          <div ng-repeat="s in schedulestack.v2">
                            <stack content="s"></stack>
                          </div>
                        </div>
                        <div class="col-xs-12 hide-md center">
                          <h4 ng-if="schedulestack.v3.length&gt;0">Chanakya Hall</h4>
                        </div>
                        <div class="col-xs-12 col-md" style="padding-top:20px;">
                          <div ng-repeat="s in schedulestack.v3">
                            <stack content="s"></stack>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="coc">
              <div class="tabstuff pad_l">
                <div class="container-fluid">
                  <div class="row middle-xs">
                    <div class="col-xs-12 col-md-3 center"><img src="./img/icons/coc.png" style="width:100px; border-radius: 50%;"/></div>
                    <div class="col-xs-12 col-md-9">
                      <h1>Code of Conduct</h1>
                      <p class="justify">FOSSMeet is a community conference intended for networking and collaboration in the developer community.

                   We value the participation of each member of the FOSS community and want all attendees to have an enjoyable and fulfilling experience. Accordingly, all attendees are expected to show respect and courtesy to other attendees throughout the conference and at all conference events.

                   To make clear what is expected, all delegates/attendees, speakers, exhibitors, organizers and volunteers at the FOSSMeet event are required to conform to the following Code of Conduct. Organizers will enforce this code throughout the event.</p>
                    </div>
                    <div class="row middle-xs">
                      <div class="col-xs-12 col-md-10">
                        <h1>The Short Version</h1>
                        <p class="justify">FOSSMeet is dedicated to providing a harassment-free conference experience for everyone, regardless of gender, sexual orientation, disability, physical appearance, body size, race, or religion. We do not tolerate harassment of conference participants in any form.

         All communication should be appropriate for a professional audience including people of many different backgrounds. Sexual language and imagery is not appropriate for any conference venue, including talks.

         Be kind to others. Do not insult or put down other attendees. Behave professionally. Remember that harassment and sexist, racist, or exclusionary jokes are not appropriate for FOSSMeet.

         Attendees violating these rules may be asked to leave the conference without a refund at the sole discretion of the conference organizers.

         Thank you for helping make this a welcoming, friendly event for all.</p>
                      </div>
                      <div class="col-xs-12 col-md-2 center"><a class="bbtn" id="TheCOCLongBtn" onclick="document.getElementById('TheCOCLongVersion').style.display='block';document.getElementById('TheCOCLongBtn').style.display='none';">Read More</a></div>
                    </div>
                    <div class="row middle-xs" id="TheCOCLongVersion" style="display:none;">
                      <div class="col-xs-12 col-md-10">
                        <h1>The Longer Version</h1>
                        <p class="justify"> Harassment includes offensive verbal comments related to gender, sexual orientation, disability, physical appearance, body size, race, religion, sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention.

             Participants asked to stop any harassing behavior are expected to comply immediately.

             Exhibitors in the expo hall, sponsor or vendor booths, or similar activities are also subject to the anti-harassment policy. In particular, exhibitors should not use sexualized images, activities, or other material. Booth staff (including volunteers) should not use sexualized clothing/uniforms/costumes, or otherwise create a sexualized environment.

             Be careful in the words that you choose. Remember that sexist, racist, and other exclusionary jokes can be offensive to those around you. Excessive swearing and offensive jokes are not appropriate for conference talks like FOSSMeet.

             If a participant engages in behavior that violates this code of conduct, the conference organizers may take any action they deem appropriate, including warning the offender or expulsion from the conference with no refund.</p>
                      </div>
                    </div>
                    <div class="row middle-xs">
                      <div class="col-xs-12 col-md-12">
                        <h1>License</h1>
                        <p class="justify">This Code of Conduct was forked from the example policy from the <a href="http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy">Geek Feminism wiki, created by the Ada Initiative and other volunteers</a>. which is under a Creative Commons Zero license.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="contact" ng-controller="ContactController">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <h2 class="center">Connect with Us</h2>
                    <div ng-if="contactSend" style="padding:75px 0px;">
                      <h4 class="center" style="padding-top:15px;">Thank you for your message.</h4>
                      <h4 class="center">We'll get back to you soon.</h4>
                    </div>
                    <form name="contactform" ng-if="!contactSend">
                      <div class="formrow">
                        <input ng-model="contact.name" type="text" name="name"/>
                        <label for="name">Name</label>
                      </div>
                      <div class="formrow">
                        <input ng-model="contact.email" type="email" name="email"/>
                        <label for="email">Email</label>
                      </div>
                      <div class="formrow">
                        <textarea id="fancy-textarea" ng-model="contact.message" name="message" required></textarea>
                        <label for="message">Message</label>
                      </div>
                      <p class="error" ng-if="(contactform.message.$dirty||submitted)&&contactform.message.$invalid">Please enter your message.</p>
                      <div class="row">
                        <div class="col-xs" ng-if="!contactLoading">
                          <button type="submit" tabindex="0" ng-click="addFeed()">Submit</button>
                        </div>
                        <div class="col-xs" ng-if="contactLoading">
                          <loader></loader>
                        </div>
                      </div>
                    </form>
                    <div class="center"><a class="social-link" href="https://poddery.com/u/fossmeet"><i class="fa fa-asterisk"></i></a><a class="social-link" href="https://www.facebook.com/fossmeet"><i class="fa fa-facebook"></i></a><a class="social-link" href="https://www.twitter.com/fossmeet"><i class="fa fa-twitter"></i></a><a class="social-link" href="https://www.gitlab.com/fosscell"><i class="fa fa-gitlab"></i></a><a class="social-link" href="https://www.youtube.com/channel/UCwfNynjOSd3oPwIFqduJ9-Q"><i class="fa fa-youtube"></i></a><a class="social-link" href="http://webchat.freenode.net/?channels=%23FOSSCell%2C%23FOSSMeet&amp;randomnick=1"><i class="fa fa-hashtag"></i></a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="modal fade" id="helpMeModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">×</button>
            <h4 class="modal-title">
              <!-- intentionally left blank-->
            </h4>
          </div>
          <div class="modal-body">
            <!-- intentionally left blank-->
          </div>
          <!--
          <div class="modal-footer">
          &nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          -->
        </div>
      </div>
    </div>
    <script type="text/javascript" src="js/config.js"></script>
    <script type="text/javascript" src="./js/vendor.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
  </body>
</html>