<!DOCTYPE html>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="keywords" content="nitc, fossmeet, NITC, CSEA, FOSSCell, fossmeet, fossmeet 2017, nit, fossmeet nit, nit calicut"/>
    <meta name="author" content="FOSSCell, NITCalicut"/>
    <meta property="og:title" content="FOSSMeet '17"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:url" content="http://fossmeet.in/2017/"/>
    <meta property="og:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta property="og:locale" content="en_IN"/>
    <meta property="og:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta property="fb:app_id" content="1063442297042156"/>
    <!-- Twitter Cards-->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@fossmeet"/>
    <meta name="twitter:creator" content="@fosscell"/>
    <meta name="twitter:title" content="FOSSMeet '17"/>
    <meta name="twitter:description" content="FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut."/>
    <meta name="twitter:image" content="http://fossmeet.in/2017/img/yash2696.png"/>
    <meta name="twitter:image:alt" content="FOSSMeet '17"/>
    <!-- Google Structured data markup-->
    <script type="application/ld+json">{"@context":"http://schema.org","@type":"Event","name":"FOSSMeet 2017","image":"http://fossmeet.in/2017/img/yash2696.png","description":"FOSSMeet is the annual event on Free and Open Source Software, conducted at National Institute of Technology, Calicut.","startDate":"2017-03-10T15:00:00+05:30","endDate":"2017-03-12T18:00:00+05:30","url":"http://fossmeet.in/2017/","location":{"@type":"Place","name":"National Institute of Technology Calicut","address":"NITC CAMPUS PO, Kettangal Koduvally Road Kozhikode, Kerala - 673601"}}</script>
    <!-- the Title of the Page-->
    <title> FOSSMeet 17 | FOSSCell NITC </title>
    <link rel="stylesheet" href="css/vendor.css" type="text/css"/><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5shiv.js"></script>
<script type="text/javascript" src="js/respond.js"></script>
<![endif]-->
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    <!-- the MANIFEST file-->
    <link rel="manifest" href="manifest.json"/>
    <meta name="msapplication-config" content="browserconfig.xml"/>
    <!-- http://realfavicongenerator.net/-->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16"/>
    <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5"/>
    <link rel="shortcut icon" href="img/favicons/favicon.ico"/>
    <meta name="apple-mobile-web-app-title" content="FOSSMeet '17"/>
    <meta name="application-name" content="FOSSMeet '17"/>
    <meta name="theme-color" content="#4285f4"/>
    @yield('meta')

  </head>
  <body ng-app="app">

    @yield('content')

    <script type="text/javascript" src="js/config.js"></script>
    <script type="text/javascript" src="./bower_components/jquery/dist/jquery.js"></script>
    <!--script(type='text/javascript', src='./bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js')-->
    <script type="text/javascript" src="./bower_components/tether/dist/js/tether.js"></script>
    <script type="text/javascript" src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./bower_components/dynamics.js/lib/dynamics.min.js"></script>
    <script type="text/javascript" src="./bower_components/angular/angular.min.js"></script>
    <script type="text/javascript" src="./bower_components/angular-resource/angular-resource.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
  </body>
</html>
