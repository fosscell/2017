<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Language" content="en"/>
    <meta name="viewport" content="width=device-width"/>
    <meta property="og:title" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:type" content="article"/>
    <meta property="fb:app_id" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <title>FossMeet Administrator</title>
    <link rel="stylesheet" href="./apps/admin/vendor.css"/>
    <link rel="stylesheet" href="./apps/admin/styles.css"/>
  </head>
  <body ng-app="app">
    <div ui-view></div>
    <script src="./apps/admin/vendor.js"></script>
    <script src="./apps/admin/app.js"></script>
  </body>
</html>
