<?php
  namespace App\Services;

  use Instamojo;

  class OnlinePayment {

    static function init(){
      $API_KEY = config('app.API_KEY');
      $AUTH_TOKEN = config('app.AUTH_TOKEN');
      $API_SERVER = config('app.API_SERVER');
      if($API_SERVER == true){
        $api = new Instamojo\Instamojo($API_KEY, $AUTH_TOKEN);
      }
      else{
        $api = new Instamojo\Instamojo($API_KEY, $AUTH_TOKEN, 'https://test.instamojo.com/api/1.1/');
      }
      return $api;
    }

    static function get_payment_url($array){
      $api = self::init();
      try {
          $response = $api->paymentRequestCreate($array);
          // print_r($response);
      }
      catch (Exception $e) {
        //  print('Error: ' . $e->getMessage());
      }
      return $response;
    }

    static function get_payment_status($id){
      $api = self::init();
      try {
          $response = $api->paymentRequestStatus(['PAYMENT REQUEST ID']);
          // print_r($response);
      }
      catch (Exception $e) {
          // print('Error: ' . $e->getMessage());
      }
      return $response;
    }


  }
?>
