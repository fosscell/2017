<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Registration extends Model{

    protected $table = 'registration';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'email', 'mobile', 'organization_name', 'gender', 'food_preference', 'tshirt_size', 'instamojo_before_payment_id', 'instamojo_after_payment_id', 'is_online_reg', 'accomodation', 'reg_type'];
    protected $hidden = [];
    public $timestamps = false;

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
