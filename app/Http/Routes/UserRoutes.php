<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'user'], function(){

		Route::post('search', 'Api\UserController@search');
		Route::get('noncreated', 'Api\UserController@noncreated');
		
		Route::get('all', 'Api\UserController@all');
		Route::post('create', 'Api\UserController@create');
		Route::get('{id}','Api\UserController@get');
		Route::post('{id}', 'Api\UserController@update');
		Route::post('{id}/delete', 'Api\UserController@delete');

	});
});
