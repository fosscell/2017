<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'workshop'], function(){

		Route::post('search', 'Api\WorkshopController@search');

		Route::get('all', 'Api\WorkshopController@all');
		Route::post('create', 'Api\WorkshopController@create');
		Route::get('{id}','Api\WorkshopController@get');
		Route::post('{id}', 'Api\WorkshopController@update');
		Route::post('{id}/delete', 'Api\WorkshopController@delete');

	});
});
