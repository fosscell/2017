<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'feed'], function(){

		Route::post('search', 'Api\FeedController@search');

		Route::get('all', 'Api\FeedController@all');
		Route::post('create', 'Api\FeedController@create');
		Route::get('{id}','Api\FeedController@get');
		Route::post('{id}', 'Api\FeedController@update');
		Route::post('{id}/delete', 'Api\FeedController@delete');

	});
});
