<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'workshopreg'], function(){

		Route::post('search', 'Api\WorkshopRegController@search');

		Route::get('all', 'Api\WorkshopRegController@all');
		Route::post('create', 'Api\WorkshopRegController@create');
		Route::get('{id}','Api\WorkshopRegController@get');
		Route::post('{id}', 'Api\WorkshopRegController@update');
		Route::post('{id}/delete', 'Api\WorkshopRegController@delete');

	});
});
