<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'registration'], function(){

        Route::post('register', 'Api\RegistrationController@register');
        Route::get('status/{id}', 'Api\RegistrationController@get_payment_status');

        Route::post('search', 'Api\RegistrationController@search');
		Route::get('all', 'Api\RegistrationController@all');
		Route::get('{id}','Api\RegistrationController@get');
		/*Route::post('{id}', 'Api\RegistrationController@update');
		Route::post('{id}/delete', 'Api\RegistrationController@delete');*/

	});
});
