<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Feed;

class FeedController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
       $this->middleware('jwt.auth', ['except'=>['create']]);
    }

	public function all(){
		return Feed::paginate($this->paginate_count);
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();

$name=$data['name'];
$email=$data['email'];
$message=$data['message'];
$toemail="info@fossmeet.in";
$subject="Website Contact Form";
$htmlContent = '
    <html>
    <body>
        <h1>message from Contact Form</h1>
        <table cellspacing="0" style="border: 2px dashed #FB4314; width: 300px; height: 200px;">
            <tr>
                <th>Name:</th><td>'.$name.'</td>
            </tr>
            <tr style="background-color: #e0e0e0;">
                <th>Email:</th><td>'.$email.'</td>
            </tr>
            <tr>
                <th>Message:</th><td>'.$message.'</td>
            </tr>
        </table>
    </body>
    </html>';

// Set content-type header for sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// Additional headers
$headers .= 'From: '.$name.'<'.$email.'>' . "\r\n";
//$headers .= 'Cc: shrimadhav@fossmeet.in' . "\r\n";
$headers .= 'Bcc: shrimadhavuk@gmail.com' . "\r\n";

$mailsendStatus = mail($toemail,$subject,$htmlContent,$headers);

        return Feed::create($data);
    }

    public function get($id){
        return Feed::findOrFail($id);
    }

	public function update($id, Request $request){
        $feed =  Feed::findOrFail($id);
        $feed->update($request->all()); //Or ->save()
        return $feed;

	}

    public function delete($id){
        if(Feed::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Feed::paginate($this->paginate_count);
        }else{
            $result = Feed::where('id','=',$request->term)

                                    ->orWhere('name','LIKE','%'.$request->term.'%')

                                    ->orWhere('email','LIKE','%'.$request->term.'%')

                                    ->orWhere('message','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'message' => 'required'

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
