<?php
  namespace App\Http\Controllers\Api;

  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use Carbon\Carbon;
  use Illuminate\Http\Request;
  use Validator;

  class GetExternalAPI extends Controller {

    private $baseurl = "https://fossmeet-nitc.talkfunnel.com/2017";

    function get_Schedule(){
      $response = file_get_contents($this->baseurl . "/schedule/json");
      $json = json_decode($response, TRUE);
      $rooms = [];
      foreach ($json['rooms'] as $value) {
        $rooms[$value['name']] = $value;
      }

      $rr = [
        'rooms' => $rooms,
        'schedule' => $json['schedule']
      ];

      return response()->json($rr, 200);
    }

    function send_EMail($fn, $fe, $t, $s, $m){
      $fromname = $fn;
      $frommailid = $fe;
      $tomailid = $t;
      $mailsubject = $s;
      $mssg = $m;
      // => https://css-tricks.com/sending-nice-html-email-with-php/
    	$message = '<html><body>';
    	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    	$message .= "<tr><td style='background: #eee;'><strong>name:</strong> </td><td>" . strip_tags($fromname) . "</td></tr>";
    	$message .= "<tr><td><strong>e-mail:</strong> </td><td style='background: #eee;'>" . strip_tags($frommailid) . "</td></tr>";
    	$message .= "<tr><td style='background: #eee;'><strong>message:</strong> </td><td>" . htmlentities($mssg) . "</td></tr>";
    	$message .= "</table>";
    	$message .= "</body></html>";
      $email_body = $message;
      // To send HTML mail, the Content-type header must be set
      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=ISO-8859-1' . "\r\n";
      $headers .= "From: " . $fromname . "<" . $frommailid . ">" . "\r\n";
      $headers .= "Reply-To: " . $fromname . "<" . $frommailid . ">" . "\r\n";
      $headers .= "X-Mailer: PHP/" . phpversion();
      $mail_response = mail($tomailid, $mailsubject, $email_body, $headers);
      if($mail_response) {
        $response['status'] = 0;
        $response['condition'] = "success";
      }
      else {
        $response['status'] = 1;
        $response['condition'] = "fail";
      }
      $returnvalue = json_encode($response);
      return response()->json(json_decode($returnvalue), 200);
    }
  }
?>
