<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use DB;

class UserController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
       $this->middleware('jwt.auth', []);
    }

	public function all(){
		return User::paginate($this->paginate_count);
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        return User::create($data);
    }

    public function get($id){
        return User::findOrFail($id);
    }

	public function update($id, Request $request){
        $user =  User::findOrFail($id);
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user->update($data);
        return $user;

	}

    public function delete($id){
        if(User::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = User::paginate($this->paginate_count);
        }else{
            $result = User::where('id','=',$request->term)

                                    ->orWhere('email','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    public function noncreated(){
        return User::where('type', User::TYPE_EMPLOYEE)
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                              ->from('employees')
                              ->whereRaw('users.id = employees.id');
                    })->get();
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'email' => 'required',

                'password' => 'required',

                'name' => 'required'

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
