<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Config;

class ConfigController extends Controller {



    public function __construct(){
       //$this->middleware('jwt.auth', []);
    }

	public function config(){
        return Config::get('config');
    }

}
