<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Registration;
use App\Models\WorkshopReg;
use App\Services\OnlinePayment;
use Redirect;

class RegistrationController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
       //$this->middleware('jwt.auth', []);
    }

	public function all(){
		return Registration::paginate($this->paginate_count);
	}

    public function complete(Request $request){

        // $request->session()->put('reg_id', 9);

        if ($request->session()->has('reg_id')) {
            //verify payment @maddy!
            return view('completereg');
        }else{
            return view('index');
        }

    }

    public function preference(Request $request){

        // $request->session()->put('reg_id', 9);

        if ($request->session()->has('reg_id')) {


            $regid = $request->session()->get('reg_id');

            $data = $request->all();

            $regs = [];

            foreach ($data as $value) {
                if(isset($value['checked'])&&$value['checked']=="true"){
                    $regs[] = [
                        'reg_id' => $regid,
                        'workshop_id' => $value['id']
                    ];
                }
            }


            if(WorkshopReg::insert($regs)){
                return response()->json(['status' => 'success'], 200);
            }else{
                return response()->json(['errors' => ['someting went wrong.']], 400);
            }



        }else{
            return response()->json(['errors' => ['reg id not found']], 400);
        }
    }



    public function register(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        $regtype = $data['reg_type'];
        // $amount = 0;
        if($regtype == 'TYPE_NITC'){
          $amount = config('app.REG_AMT_NITC');
        }
        else if($regtype == 'TYPE_OTHER'){
          $amount = config('app.REG_AMT_OTHERS');
        }
        else if($regtype == 'TYPE_PROFESSIONAL'){
          $amount = config('app.REG_AMT_PROFESSIONALS');
        }
        $array = array(
          "purpose" => "FOSSMeet 17 Registrations",
          "amount" => $amount,
          "send_email" => true,
          "email" => $data['email'],
          "buyer_name" => $data['name'],
          "phone" => $data['mobile'],
          "send_sms" => false,
          //"redirect_url" => "http://localhost:8000/complete",
          "redirect_url" => "http://fossmeet.in/2017/public/complete",
          "allow_repeated_payments" => false
        );
        $data['is_online_reg'] = 1;
        $OnlinePaymentResponse = OnlinePayment::get_payment_url($array);
        $data['instamojo_before_payment_id'] = $OnlinePaymentResponse['id'];
        $reg = Registration::create($data);
        $reg->longUrl = $OnlinePaymentResponse['longurl'];
        //return $reg;
        //let's redirect!
        $request->session()->put('reg_id', $reg->id);
        return Redirect::to($OnlinePaymentResponse['longurl']);
    }

    public function get_payment_status($id){
      // instamojo_before_payment_id
      return OnlinePayment::get_payment_status($id);
    }

    public function get($id){
        return Registration::findOrFail($id);
    }

	public function update($id, Request $request){
        $registration =  Registration::findOrFail($id);
        $registration->update($request->all()); //Or ->save()
        return $registration;

	}

    public function delete($id){
        if(Registration::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Registration::paginate($this->paginate_count);
        }else{
            $result = Registration::where('id','=',$request->term)

                                    ->orWhere('name','LIKE','%'.$request->term.'%')

                                    ->orWhere('email','LIKE','%'.$request->term.'%')

                                    ->orWhere('mobile','LIKE','%'.$request->term.'%')

                                    ->orWhere('reg_type','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'name' => 'required',

                'email' => 'required',

                'mobile' => 'required',

                'organization_name' => 'required',

                'gender' => 'required',

                'food_preference' => 'required',

                'accomodation' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
