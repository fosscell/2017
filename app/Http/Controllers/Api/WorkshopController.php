<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Workshop;

class WorkshopController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
       $this->middleware('jwt.auth', ['except' => ['all', 'get']]);
    }

	public function all(){
		return Workshop::all();
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return Workshop::create($data);
    }

    public function get($id){
        return Workshop::findOrFail($id);
    }

	public function update($id, Request $request){
        $workshop =  Workshop::findOrFail($id);
        $workshop->update($request->all()); //Or ->save()
        return $workshop;

	}

    public function delete($id){
        if(Workshop::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Workshop::paginate($this->paginate_count);
        }else{
            $result = Workshop::where('id','=',$request->term)

                                    ->orWhere('name','LIKE','%'.$request->term.'%')

                                    ->orWhere('count','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'name' => 'required',

                'count' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
