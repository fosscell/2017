<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\WorkshopReg;

class WorkshopRegController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
       //$this->middleware('jwt.auth', []);
    }

	public function all(){
		return WorkshopReg::paginate($this->paginate_count);
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return WorkshopReg::create($data);
    }

    public function get($id){
        return WorkshopReg::findOrFail($id);
    }

	public function update($id, Request $request){
        $workshopreg =  WorkshopReg::findOrFail($id);
        $workshopreg->update($request->all()); //Or ->save()
        return $workshopreg;

	}

    public function delete($id){
        if(WorkshopReg::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = WorkshopReg::paginate($this->paginate_count);
        }else{
            $result = WorkshopReg::where('id','=',$request->term)
                                
                                    ->orWhere('reg_id','LIKE','%'.$request->term.'%')
                                
                                    ->orWhere('workshop_id','LIKE','%'.$request->term.'%')
                                
                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [
            
                'reg_id' => 'required',
            
                'workshop_id' => 'required',
            
        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
