<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use Validator;
use Hash;
use Config;

class AuthController extends Controller {


	public function __construct(){
		$this->middleware('jwt.auth', ['except' => ['authenticate']]);
	}

	public function authenticate(Request $request){
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['errors' => ['Wrong Username/Password.']], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['errors' => ['could_not_create_token']], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(['user'=>Auth::user(),'token'=>$token,'config' => Config::get('config')]);
    }

	public function refresh(){
		$token = JWTAuth::getToken();
		if(!$token){
			return response()->json(['errors' => ['no token!']], 401);
		}
		try{
			$token = JWTAUTH::refresh($token);
		}catch(TokenInvalidException $e){
			return response()->json(['errors' => ['access denied.']], 401);
		}
		return response()->json(['user'=>Auth::user(),'token'=>$token,'config' => Config::get('config')]);
	}

	public function changepassword(Request $request){
		$user = Auth::user();
		$rules = [
          'current_password' => 'required',
          'new_password' => 'required',
        ];
		$values = $request->all();

        $validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }

		if(Hash::check($values['current_password'], $user->password)){
			$user->password = bcrypt($request->new_password);
			$user->save();
		}else{
			return response()->json(['errors' => ['Wrong Current Password.']], 401);
		}

		return $user;
	}



}
