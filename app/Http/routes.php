<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/admin', function () {
    return view('admin');
});

Route::group(['prefix' => 'api'], function(){

	Route::post('auth', 'Auth\AuthController@authenticate');
	Route::post('refreshtoken', 'Auth\AuthController@refresh');
	Route::post('password', 'Auth\AuthController@changepassword');
	Route::get('config', 'Api\ConfigController@config');

    Route::post('preference', 'Api\RegistrationController@preference');

});

Route::post('register', 'Api\RegistrationController@register');
Route::get('complete', 'Api\RegistrationController@complete');


require('Routes/UserRoutes.php');
require('Routes/RegistrationRoutes.php');
require('Routes/WorkshopRoutes.php');
require('Routes/WorkshopRegRoutes.php');
require('Routes/TalkFunnelSchedule.php');
require('Routes/FeedRoutes.php');
