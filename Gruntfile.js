module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      main: {
          files : [{
              src : ['Gruntfile.js', 'frontend/js/**/*.js']
          }]
      },
      options: {
        globals: {
          'jQuery': true,
          'angular': true,
          'console': true,
          '$': true,
          '_': true,
          'moment': true
        }
      }
    },

    htmlhint: {
          options: {
            'attr-lower-case': true,
            'attr-value-not-empty': false,
            'tag-pair': true,
            'tag-self-close': true,
            'tagname-lowercase': true,
            'id-class-value': true,
            'id-unique': true,
            'img-alt-require': true,
            'img-alt-not-empty': true
          },
          main: {
            src: ['public/index.html']
          }
    },

    sass: {
      options: {
          style : 'compressed',
          loadPath:[
              './public/bower_components/',
              './public/bower_components/bootstrap-sass/assets/stylesheets/'
          ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'frontend/sass',
          src: ['styles.scss'],
          dest: 'public/css',
          ext: '.css'
        }]
      },
      vendor:{
        files: [{
          expand: true,
          cwd: 'frontend/sass',
          src: ['vendor.scss'],
          dest: 'public/css',
          ext: '.css'
        }]
      }
    },
    concat: {
      config: {
        src: ['frontend/js/config.js'],
        dest: 'public/js/config.js',
      },
      functions: {
          src : ["frontend/js/functions/**.js"],
          dest : 'public/js/functions.js'
      },
      app: {
        src: ['frontend/js/app/**'],
        dest: 'public/js/app.js',
    },
    vendor : {
        src : [
            'public/bower_components/jquery/dist/jquery.min.js',
            'public/bower_components/tether/dist/js/tether.min.js',
            'public/bower_components/bootstrap/dist/js/bootstrap.min.js',
            'public/bower_components/dynamics.js/lib/dynamics.min.js',
            'public/bower_components/angular/angular.min.js',
            'public/bower_components/angular-resource/angular-resource.min.js'
        ],
        dest : 'public/js/vendor.js'
    }

    },

    copy: {
      fonts:{
        expand: true,
        files:[
          {expand: true, cwd: 'public/bower_components/font-awesome/fonts', src:"**", dest:'public/fonts'}
        ]
      }
    },


    pug: {
        options: {
          pretty: true,
          data: {
            debug: true
          }
      },
      compile: {
        files: [
          {expand: true, cwd: 'frontend', src:"*.pug", dest:'resources/views', ext: '.blade.php'}
        ]
     },
     templates : {
         files :  [{
             expand: true,
             cwd: 'frontend/templates',
             src: 'directives/**.pug',
             dest: 'public/templates',
             ext: '.html'
         }]
     }
    },

    watch: {
         grunt: {
           files: ['Gruntfile.js'],
           options: {
             nospawn: true,
             keepalive: true,
             livereload: true
           },
           tasks: ['public:dev']
         },
         html: {
             files: ['resources/views/welcome.blade.php'],
             options: {
                 nospawn: true,
                 keepalive: true,
                 livereload: true
             },
             tasks: ['htmlhint']
         },
         js: {
           files: 'frontend/js/**',
           options: {
             nospawn: true,
             livereload: true
           },
           tasks: ['jshint', 'concat']
         },
         pug: {
           files: ['frontend/*.pug', 'frontend/templates/**/**.pug', 'frontend/templates/**.pug'],
           options: {
             nospawn: true,
             livereload: true
           },
           tasks: [ 'pug', 'preprocess:dev']
         },
         sass: {
           files: ['frontend/sass/**'],
           options: {
             nospawn: true,
             livereload: true
           },
           tasks: ['sass:dist']
         }

    },

    preprocess: {
          dev: {
            options: {
              context: {
                MODE: 'dev',
                BUILD_TS: '<%= ((new Date()).valueOf().toString()) + (Math.floor((Math.random()*1000000)+1).toString()) %>'
              }
            },
            src: 'resources/welcome.blade.php',
            dest: 'resources/welcome.blade.php'
          }
    },


    connect: {
        server: {
            options: {
                port: 8080,
                base: 'public',
                hostname: 'localhost',
                livereload: true,
                middleware: function (connect, options, middlewares) {
                  middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                  return middlewares;
                }
            },
            proxies: [
                {
                context: '/api',
                host: 'localhost',
                port: 80,
                changeOrigin: true,
                rewrite: {
                    '^/api': '/foss_backend/public/api'
                }
              }
            ]
        }
    }

  });


  grunt.registerTask('default', ['jshint']);

  grunt.registerTask('public:dev', function (target) {
       grunt.task.run([
         'htmlhint',
         'sass',
         'pug',
        //  'preprocess:dev',
         'concat',
         'jshint'
       ]);
   });

  grunt.registerTask('server', function (target) {
       grunt.task.run([
           'configureProxies:server',
           'connect',
           'watch'
       ]);
   });

};
