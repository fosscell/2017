function anim() {
    var mrefreshinterval = 2000;
    var distance=0;

    var el = document.querySelector(".imgtile");

    var $home = $("#home");
    var home_offset = $home.offset();
    console.log(home_offset);

    var $tiles = $(".imgtile");
    var offsets = [];
    var elements = [];
    $tiles.each(function(index,element){
        offsets.push({
            left : $(element).offset().left - home_offset.left,
            top : $(element).offset().top - home_offset.top
        });
        elements.push(element);
    });
    console.log('offsets');
    console.log(offsets);


    var center = {
        x : $home.width()/2,
        y : $home.height()/2
    };

    console.log(center);

    var el_offset = $(el).offset();

    var calc_dist = function(x,y){
        return Math.sqrt(x*x+y*y);
    };

    var xCo = function(x){
        return x - home_offset.left;
    };

    var yCo = function(y){
        return y - home_offset.top;
    };

    function update(pageX,pageY){

        var mousex = xCo(pageX);
        var mousey = yCo(pageY);

        var ch_x = (mousex-center.x);
        var ch_y = (mousey-center.y);

        var angle = Math.atan2(mousex, mousey);


        for (var i = 0; i < elements.length; i++) {

            var d_p = calc_dist(Math.abs(mousex-offsets[i].left),Math.abs(mousey-offsets[i].top))/4;
            var ratio = 0.5;
            var d_x = offsets[i].left - (ch_x+10)*ratio+d_p*Math.sin(angle);
            var d_y = offsets[i].top - (ch_y+10)*ratio+d_p*Math.cos(angle);


            dynamics.css(elements[i], {
              translateX: d_x,
              translateY: d_y
            });
        }

    }

    update(0,0);


    $('#home').mousemove(function(e) {
        update(e.pageX,e.pageY);
    });


}

anim();
