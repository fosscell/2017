
var DaysLeft = function(year, month, date, hour, minute, second){
  var yob = new Date(year, month - 1, date, hour, minute, second);
  var todaydate = new Date();

  var differentdates = yob - todaydate;

  var one_sec = 1000;
  var one_min = 60;
  var one_hour = 60;
  var one_day = 24;
  var one_year = 365;

  var x = differentdates / one_sec;

  var seconds = Math.floor(x % one_min);
  x = x / one_min;
  var minutes = Math.floor(x % one_hour);
  x = x / one_hour;
  var hours = Math.floor(x % one_day);
  x = (x / one_day);
  var days = Math.floor(x % one_year);
  x = x / one_year;
  var years = Math.floor(x);

  return {
    'y': years,
    'd': days,
    'H': hours,
    'M': minutes,
    'S': seconds
  };
};

// => update age
setInterval(function(){
  if(document.getElementById('time_countdown')){
    var countup = DaysLeft(2017,02,24,15,30,30);
    var years = countup.y;
    var days = countup.d;
    var hours = countup.H;
    var minutes = countup.M;
    var seconds = countup.S;
    document.getElementById('yrsl').innerHTML = "<span class='digit'>" + years.toString().split('').join('</span><span class="digit">') + "</span>";
    document.getElementById('daysl').innerHTML = "<span class='digit'>" + days.toString().split('').join('</span><span class="digit">') + "</span>";
    document.getElementById('hrsl').innerHTML = "<span class='digit'>" + hours.toString().split('').join('</span><span class="digit">') + "</span>";
    document.getElementById('minl').innerHTML = "<span class='digit'>" + minutes.toString().split('').join('</span><span class="digit">') + "</span>";
    document.getElementById('secl').innerHTML = "<span class='digit'>" + seconds.toString().split('').join('</span><span class="digit">') + "</span>";
  }
}, 1000);
