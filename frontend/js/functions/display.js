var DisplayMessageInModal = function(heading, message){
  $('.modal-body').html(message);
  $('.modal-title').html(heading);
  $($("#helpMeModal").modal());
};

var DisplaySpecialLink = function(url, desc){
  DisplayMessageInModal(desc, "<iframe src='" + url + "' width='100%' height='100%' />");
};

var PreviewURL = function(url){
  DisplaySpecialLink(url, "Preview WebPage in this Single Page Application");
};
