$(function(){
  var hash = window.location.hash;
  if(hash){
      $('ul.sidebar-nav a[href="' + hash + '"]').tab('show');
  }
});

$('.sidebar-nav li a').click(function (e) {
  e.preventDefault();
  console.log('clickman');
  var scrollmem = $('body').scrollTop() || $('html').scrollTop();
  window.location.hash = this.hash;
  $('html,body').scrollTop(scrollmem);
  $(this).tab('show');
});
