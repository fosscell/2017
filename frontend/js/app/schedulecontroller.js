(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ScheduleController', function($scope, BackEnd){


        $scope.in = {};

        $scope.$watch('in.day', function (newValue, oldValue) {
            console.log(newValue);
            if($scope.in.day)
                sort($scope.in.day);
        });

        var sort = function (index) {
                var slots = $scope.schedule[index].slots;
                var venue = {
                    v1 : [], //ssl
                    v2 : [], //nsl
                    v3 : [] //chanakya
                };
                for (var i = 0; i < slots.length; i++) {
                    var sessions = slots[i].sessions;
                    for (var j = 0; j < sessions.length; j++) {
                        var s = sessions[j];
                        var ses = {
                            //venue : $scope.schedule.rooms[s.room].title,
                            start: s.start,
                            end: s.end,
                            title : s.title
                        };
                        var rand = Math.random();

                        if(s.room=='nitc/software-systems-laboratory-ssl-nitc'){
                            venue.v1.push(s);
                        }else if(s.room=='nitc/network-systems-lab-nsl-nitc'){
                            venue.v2.push(s);
                        }else{
                            venue.v3.push(s);
                        }
                    }
                }
                $scope.schedulestack = venue;
        };


      BackEnd.schedule(function(res){
          console.log(res);
          $scope.schedule = res.schedule;
          $scope.in.day = '0';
          sort(0);
      },function(err){
        console.log(err);
      });
    });

})();
