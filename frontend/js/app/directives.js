(function() {

    'use strict';

    var app = angular.module('directives', [
    ]);

    app.directive('loader', function(){
        return {
            restrict: 'E',
            scope:{
            },
            link: function(scope, element, attr) {

            },
            template: '<div class="loader"></div>'
        };
    });

    app.directive('radio', function(){
        return {
            restrict: 'E',
            scope:{
                model : '=',
                value : '@',
                label : '@'
            },
            link: function(scope, element, attr) {

                scope.set = function () {
                    if(scope.model==scope.value){
                        scope.model = undefined;
                        return;
                    }
                    scope.model = scope.value;
                };

            },
            templateUrl: './templates/directives/radio.html'
        };
    });

    app.directive('stack', function(){
        return {
            restrict: 'E',
            scope:{
                content : '='
            },
            link: function(scope, element, attr) {
                scope.title = scope.content.title;
                scope.start = scope.content.start;
                scope.end = scope.content.end;
                scope.height = 2;
                scope.color = 'red';
                scope.type='stack';
            },
            templateUrl: './templates/directives/stack.html'
        };
    });




})();
