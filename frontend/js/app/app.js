
var app = angular.module('app', [
  'ngResource',
  'directives'
]);

var baseURL = "./api";

app.factory('BackEnd',
  function ($resource){
      return $resource(baseURL + "/schedule/all", {querytype: '@querytype', id : '@id'},{
          schedule: {method: 'GET', cache: false, isArray: false},
          registerStart: {method:'POST',cache:false,isArray:false,url:baseURL + '/registration/register'},
          PreferenceStat: {method:'POST',cache:false,isArray:false,url:baseURL + '/WorkshopPreference'},
          SendEMail: {method:'POST',cache:false,isArray:false,url:baseURL + '/mail'},
          checkStatus: {method:'POST',cache:false,isArray:true,url:baseURL + '/registration/status/:id'},
          preference : {method:'POST',cache:false,isArray:false,url:baseURL + '/preference'},
          workshops : {method:'GET',cache:false,isArray:true,url:baseURL + '/workshop/all'},
          feedback : {method:'POST', isArray:false, url:baseURL + '/feed/create'}
      });
      
  });
