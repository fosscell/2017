(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('PreferenceController', function ($scope, BackEnd, $window) {
        $scope.in = {}; //inputs
        $scope.isLoading = false;

        BackEnd.workshops(function (res) {
            $scope.workshops = res;
            $scope.fetched = true;
            console.log(res);
        }, function (err) {
            console.log(err);
        });

        $scope.register = function () {
            $scope.isLoading = true;
            console.log($scope.workshops);
            BackEnd.preference($scope.workshops, function (res) {
                $scope.isLoading = false;
                $scope.isDone = true;
                console.log(res);
            }, function (err) {
                console.log(err);
            });
        };

    });

})();
