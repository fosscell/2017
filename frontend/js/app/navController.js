(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('NavController', function ($scope) {
        $scope.isOpen = false;
        $scope.toggle = function () {
                $scope.isOpen = !$scope.isOpen;
                console.log('toggle');
        };
    });

})();
