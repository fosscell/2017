(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('RegController', function ($scope, BackEnd, $window) {
        $scope.in = {}; //inputs
        $scope.isLoading = false;
        $scope.showForm = false;
        $scope.register = function ($event) {
            $scope.submitted = true;
            if( !$scope.in.name ||
                !$scope.in.email ||
                !$scope.in.mobile ||
                !$scope.in.reg_type ||
                !$scope.in.organization_name||
                !$scope.in.gender ||
                !$scope.in.food_preference||
                !$scope.in.accomodation
            ){
                return false;
            }else{
                post('./register',$scope.in,"post");
            }


        };

        $scope.show = function () {
            $scope.showForm = true;
        };

        $scope.$watch('in.reg_type', function(oldV,newV) {
            console.log('watch!!');
            if($scope.in.reg_type=='TYPE_NITC'){
                $scope.in.organization_name = 'NITC';
            }else{
                $scope.in.organization_name = undefined;
            }
        });


        var post = function(path, params, method) {
                method = method || "post";

                var form = document.createElement("form");
                form.setAttribute("method", method);
                form.setAttribute("action", path);
                for(var key in params) {
                    if(params.hasOwnProperty(key)) {
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", key);
                        hiddenField.setAttribute("value", params[key]);
                        form.appendChild(hiddenField);
                     }
                }
                document.body.appendChild(form);
                form.submit();
        };

    });

})();
