(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ContactController', function ($scope, BackEnd, $window) {
        console.log('contact');
        $scope.contact = {}; //inputs
        $scope.contactLoading = false;
        $scope.contactSend = false;
        $scope.addFeed = function ($event) {
            $scope.submitted = true;
            if(!$scope.contact.message){
                return false;
            }else{
                $scope.contactLoading = true;
                console.log($scope.contact);
                BackEnd.feedback($scope.contact, function (res) {
                    console.log(res);
                    $scope.contactSend = true;
                }, function (err) {
                    console.log(err);
                });
            }
        };


    });

})();
