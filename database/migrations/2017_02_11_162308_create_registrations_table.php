<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration', function (Blueprint $table) {
           $table->increments('id')->unique();
           $table->string('name');
           $table->string('email');
           $table->string('mobile');
           $table->string('organization_name');
           $table->enum('gender', ['Male', 'Female']);
           $table->enum('reg_type', ['TYPE_NITC', 'TYPE_OTHER', 'TYPE_PROFESSIONAL']);
           $table->enum('food_preference', ['veg', 'non veg']);
           $table->enum('tshirt_size', ['S', 'M', 'L', 'XL', 'XXL'])->nullable();
           $table->string('instamojo_before_payment_id')->nullable();
           $table->string('instamojo_after_payment_id')->nullable();
          //  $table->decimal('amount_paid', 7, 2);
           $table->enum('is_online_reg', ['Y', 'N']);
           $table->enum('accomodation', ['Y', 'N']);

           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('registration');
    }
}
