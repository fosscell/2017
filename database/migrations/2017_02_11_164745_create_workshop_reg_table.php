<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopRegTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshop_reg', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('reg_id')->unsigned();
            $table->integer('workshop_id')->unsigned();

            $table->foreign('reg_id')
 				   ->references('id')->on('registration')
 				   ->onDelete('cascade')
 				   ->onUpdate('cascade');

           $table->foreign('workshop_id')
				   ->references('id')->on('workshop')
				   ->onDelete('cascade')
				   ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workshop_reg');
    }
}
