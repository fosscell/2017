<?php

use App\Models\Registration;

class RegistrationTest extends TestCase {

	public $api_base = "/api/registration/";
	protected $isDebugEnabled = true;

	function __construct(){

	}


	public function sample(){
		return  [

				'name' => str_random(10),

				'email' => str_random(10),

				'mobile' => str_random(10),

				'organization_name' => str_random(10),

				'gender' => "Male",

				'reg_type' => "TYPE_NITC",

				'food_preference' => str_random(10),

				'tshirt_size' => str_random(10),

				// 'amount_paid' => str_random(10),

				'is_online_reg' => str_random(10),

				'accomodation' => str_random(10),

		];
	}

	public function get(){
		return Registration::get()->first();
	}

	public function create($content){
		return Registration::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){
		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostId(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->get();

		$response = $this->call('POST', $this->api_base.$model->id, $content, [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostDelete(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);

		$response = $this->call('POST', $this->api_base.$model->id.'/delete', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
