<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected $isDebugEnabled = false;


    public function token(){

        /*$user = User::where('email','admin@example.in')->get()->first();
        if(!$user){
            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@example.in',
                'password' => '12345',
            ]);
        }

        $token = JWTAuth::fromUser($user);
        JWTAuth::setToken($token);
        $this->refreshApplication();*/

        $server = [
            //'HTTP_Authorization' => 'Bearer '.$token,
            'Accept' => 'application/json'
        ];
        return $server;
    }

    public function json($response){
        $content = $response->getContent();
        $json = json_decode($content, TRUE);
        if($this->isDebugEnabled){
            if($response->getStatusCode()!=200){
                print_r($json['errors']);
            }else{
                print_r($json);
            }
        }

        if($response->getStatusCode()!=200){
            print_r($json['errors']);
        }

        return $json;
    }

    public function debug($d){
        print_r($d);
    }

}
