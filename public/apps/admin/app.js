
/*console.log = function(){

};*/

(function() {

	'use strict';

	var app = angular.module('app', [
			'ui.router',
			'satellizer',
			'AppControllers',
            'AppDirectives',
            'AppFilters',
			'AppServices',
			'AppConfig',
			'lumx',
			'ngAnimate',
			'ngScrollbars',
			'AppRoutes'
	]);

	angular.module('AppControllers',[]);
	angular.module('AppDirectives',['lumx']);
	angular.module('AppServices',['ngResource']);
	angular.module('AppFilters',[]);
	angular.module('AppConfig',[]);
	angular.module('AppRoutes',[
		'MainRoutes',
		'UserRoutes',
        'RegistrationRoutes',
        'WorkshopRoutes'
	]);

	app.config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, Config) {


		$authProvider.baseUrl = './';
		$authProvider.loginUrl = 'api/auth';
		$urlRouterProvider.otherwise('/home');

	}).run(function($rootScope, $state, $auth, $window, $timeout, MainConfig, Middleware) {


		$rootScope.user = {};
		$rootScope.go = function(state,params){
			$state.go(state,params);
		};

		console.log(MainConfig);

		$rootScope.$on('$stateChangeStart', function(event, toState) {

			console.log(toState);


			$("html, body").animate({scrollTop: 0}, "slow"); //not a good practice though!
		    Middleware.auth($rootScope.authenticated,event,toState);
			//Middleware.fakeIt(event,toState);


		});

	});

})();

(function() {

	'use strict';

  var config = angular.module('AppConfig');
  config.constant('Config',{
	  templates: "./apps/admin/templates/",
	  partials: './templates/partials/'

  });

  config.factory('MainConfig',function($timeout, Config, BackendConfig) {

	  var backend;

	  var resolveConfig = function(cb) {
		  console.log('resolving..');
		  if(backend){
			  cb(true);
			  return;
		  }
		  BackendConfig.get(function(res) {
			  backend = res;
			  console.log(res);
    		  cb(true);
    	  }, function (err) {
    		  cb(false);
    	  });
	  };

	  var getBackend = function(){
		  return backend; //
	  };

	  var setBackend = function(config){
		 backend = config;
	  };


	  var basicScrollConfig = {
            autoHideScrollbar: false,
            theme: 'dark',
            axis: 'y',
            advanced:{
                updateOnContentResize: true
            },
            setHeight: '50vh',
            scrollInertia: 500,
            mouseWheel:{
                scrollAmount: 250
            }
      };

	  var getScrollConfig = function(callback){
		  var config = angular.copy(basicScrollConfig);
	      config.callbacks = {
	          onTotalScroll : callback
	      };
		  return config;
	  };

	  var mainmenu = [
          {
              name: 'Create Users',
              link: 'user',
              parent : 'user',
              icon: 'user'
          },
          {
              name: 'Registrations',
              link: 'registration',
              parent : 'registration',
              icon: 'user'
          },
          {
              name: 'Workshops',
              link: 'workshop',
              parent : 'workshop',
              icon: 'user'
          },
		  {
              name : 'Logout',
              link: 'logout',
              icon: 'reply'
          }
      ];

	  var logoutmenu = [{
          name: 'Login',
          link: 'auth',
          icon: 'lock'
      }];

	  return {
		  getScrollConfig: getScrollConfig,
		  basicScrollConfig : basicScrollConfig,
		  resolveConfig : resolveConfig,
		  getBackend : getBackend,
		  setBackend : setBackend,
		  mainmenu : mainmenu,
		  logoutmenu: logoutmenu
	  };


  });

})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('NavController', function($scope, $rootScope, $timeout, $state, MainConfig) {

      $scope.isOpen = false; //!
      $scope.$state = $state;
      $scope.menu = MainConfig.logoutmenu;

      var openedViaMenuButton = false;


      $scope.$on('onLoginComplete',function(e,args) {
          $scope.menu = MainConfig.mainmenu;
      });

      $scope.$on('onLogoutComplete',function(e,args) {
          $scope.menu =MainConfig.logoutmenu;
      });

      $scope.nav = function(item){
        $scope.isOpen = false;
        if(openedViaMenuButton){
            $timeout(function(){
                $state.go(item.link);
            },1000);
        }else{
            $state.go(item.link);
        }
      };

      $scope.openMenu = function(){
          openedViaMenuButton = true;
          $scope.isOpen = !$scope.isOpen;
      };

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AuthController', function($rootScope, $scope, $state, AuthService, LBHandler, $auth, MainConfig) {

      $scope.input = {};

      $scope.$on('onLoginComplete',function(e,args) {
         $state.go('home');
      });

      //user may have already authenticated if he didn't explicitly logout(refresh case). Simply try to refresh the token!
      if(!AuthService.getExplicitLogOut()){
          console.log('Not Explicit Logout');
          $scope.needsauth = false;
          AuthService.autoLogin(function(res){
              //we have a valid token and user
              MainConfig.setBackend(res.config);

          }, function(err) {
              //failed. Display Login
              $scope.needsauth = true;
          });
      }else{
          console.log('Explicit Logout');
          $scope.needsauth = true;
      }


      $scope.loginHandler = LBHandler.getHandler(0);
      $scope.authenticate = function() {
          var credentials = {
                   email: $scope.input.email,
                   password: $scope.input.password
           };
          $scope.loginHandler.update(AuthService.login,credentials,function(res) {
            //let the event flow take care of it.
            //should be having config.
            MainConfig.setBackend(res.config);

          }, function(err) {
               $scope.errors = err.data.errors;
              console.log(err);
          });
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LogOutController', function($scope, AuthService, $state) {

      AuthService.logout(function() {
          $state.go('auth');
      });

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('RegistrationAddController', function($scope, LBHandler, Registration) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;

        $scope.handler.update(Registration.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('RegistrationController', function($scope, MainConfig, Pagination,  Registration, $state) {

      $scope.results = [];
      var paginator = Pagination.getPaginator(Registration.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('registration.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('registration.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('RegistrationEditController', function($scope, LBHandler, Registration, $timeout) {

      if(!$scope.to_edit){
          $scope.go('employee.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(Registration.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Registration.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('registration');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('RegistrationViewController', function($scope, $state, Registration, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Registration.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserAddController', function($scope, LBHandler, User, MainConfig) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(User.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserController', function($scope, MainConfig, Pagination,  User, $state) {

      $scope.userTypes = MainConfig.getBackend().userTypes;

      $scope.results = [];
      var paginator = Pagination.getPaginator(User.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('user.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('user.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserEditController', function($scope, LBHandler, User, $timeout, MainConfig) {

      if(!$scope.to_edit){
          $scope.go('user.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(User.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(User.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('user');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserViewController', function($scope, $state, User, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          User.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('WorkshopAddController', function($scope, LBHandler, Workshop) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;

        $scope.handler.update(Workshop.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('WorkshopController', function($scope, MainConfig, Pagination,  Workshop, $state) {

      $scope.results = [];
      var paginator = Pagination.getPaginator(Workshop.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('workshop.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('workshop.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('WorkshopEditController', function($scope, LBHandler, Workshop, $timeout) {

      if(!$scope.to_edit){
          $scope.go('employee.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(Workshop.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Workshop.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('workshop');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('WorkshopViewController', function($scope, $state, Workshop, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Workshop.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('loadingButton', function(Config){
            return {
                restrict: 'AEC',
                scope:{
                    handler: '=',
                    disableCriteria: '=',
                    lxType : '@',
                    lxSize : '@',
                    lxDiameter : '@',
                    lxColor: '@',
                    value : '@',
                    successText : '@',
                    failText : '@'
                },
                link: function(scope, element, attr) {
                    if(scope.handler){
                        scope.$watch(function(scope){
                            return scope.handler.getState();
                        },function(newValue, oldValue){
                            scope.state = newValue;
                        });
                    }
                },
                templateUrl: Config.templates+'directives/loadingbutton.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('appProgress', lxProgress); /*check*/

    function lxProgress(Config)
    {
        return {
            restrict: 'E',
            templateUrl: Config.templates+'directives/progress.html',
            scope:
            {
                lxColor: '@?',
                lxDiameter: '@?',
                lxType: '@',
                lxValue: '@'
            },
            controller: LxProgressController,
            controllerAs: 'lxProgress',
            bindToController: true,
            replace: true
        };
    }

    function LxProgressController()
    {
        var lxProgress = this;
        init();
        lxProgress.getCircularProgressValue = getCircularProgressValue;
        lxProgress.getLinearProgressValue = getLinearProgressValue;
        lxProgress.getProgressDiameter = getProgressDiameter;



        ////////////
        
        function getCircularProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'stroke-dasharray': lxProgress.lxValue * 1.26 + ',200'
                };
            }
        }

        function getLinearProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'transform': 'scale(' + lxProgress.lxValue / 100 + ', 1)'
                };
            }
        }

        function getProgressDiameter()
        {
            if (lxProgress.lxType === 'circular')
            {
                return {
                    'transform': 'scale(' + parseInt(lxProgress.lxDiameter) / 100 + ')'
                };
            }

            return;
        }

        function init()
        {
            lxProgress.lxDiameter = angular.isDefined(lxProgress.lxDiameter) ? lxProgress.lxDiameter : 100;
            lxProgress.lxColor = angular.isDefined(lxProgress.lxColor) ? lxProgress.lxColor : 'primary';
        }
    }
})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('stringToNumber', function(){
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                  ngModel.$parsers.push(function(value) {
                    return '' + value;
                  });
                  ngModel.$formatters.push(function(value) {
                    return parseFloat(value);
                  });
                }
            };
        });

})();


(function() {
  'use strict';
  var base = './api/config';

  var services = angular.module('AppServices');


  services.factory('BackendConfig', function($resource) {
    return $resource(base, {}, {
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/registration'; //<---

  var services = angular.module('AppServices');


  services.factory('Registration', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/user';

  var services = angular.module('AppServices');


  services.factory('User', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        },
        noncreated:{
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/noncreated'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/workshop'; //<---

  var services = angular.module('AppServices');


  services.factory('Workshop', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();


(function() {

	'use strict';
	var app = angular.module('MainRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
	        .state('app', {
	            abstract: true,
	            url: '',
	            templateUrl: Config.templates + 'base.html',
	            controller: 'NavController'
	        });

        $stateProvider
            .state('home', {
                url: '/home',
                parent : 'app',
                templateUrl: Config.templates + 'home.html',
            })

			.state('auth', {
	            url: '/login',
	            parent: 'app',
	            templateUrl: Config.templates + 'login.html',
	            controller: 'AuthController'
	        })

			.state('logout', {
	            url: '/logout',
	            parent: 'app',
	            template: '',
	            controller: 'LogOutController'
	        });
  });

})();

(function() {

	'use strict';
	var app = angular.module('RegistrationRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('registration', {
				url: '/registration',
				parent: 'app',
				templateUrl: Config.templates + 'registration/index.html',
				controller: 'RegistrationController'
			})
			.state('registration.add', {
				url: '/add',
				templateUrl: Config.templates + 'registration/add.html',
				controller: 'RegistrationAddController'
			})
			.state('registration.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'registration/edit.html',
				controller: 'RegistrationEditController'
			})
			.state('registration.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'registration/view.html',
				controller: 'RegistrationViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('UserRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('user', {
				url: '/user',
				parent: 'app',
				templateUrl: Config.templates + 'user/index.html',
				controller: 'UserController'
			})
			.state('user.add', {
				url: '/add',
				templateUrl: Config.templates + 'user/add.html',
				controller: 'UserAddController'
			})
			.state('user.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'user/edit.html',
				controller: 'UserEditController'
			})
			.state('user.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'user/view.html',
				controller: 'UserViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('WorkshopRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('workshop', {
				url: '/workshop',
				parent: 'app',
				templateUrl: Config.templates + 'workshop/index.html',
				controller: 'WorkshopController'
			})
			.state('workshop.add', {
				url: '/add',
				templateUrl: Config.templates + 'workshop/add.html',
				controller: 'WorkshopAddController'
			})
			.state('workshop.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'workshop/edit.html',
				controller: 'WorkshopEditController'
			})
			.state('workshop.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'workshop/view.html',
				controller: 'WorkshopViewController'
			});

  });

})();


(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('LBHandler', function($timeout){
            var Handler = function(delay){
                var handler = this;
                handler.delay = delay;
                handler.state = 0; //0 - start, 1 - loading, 2 - success, 3 - failed
                handler.update = function(f,data,cb,errCb){
                    handler.state = 1;
                    f(data,function(res){
                        handler.state = 2;
                        handler.reset();
                        console.log(res);
                        cb(res);
                    },function(err){
                        handler.state = 3;
                        handler.reset();
                        console.log(err);
                        errCb(err);
                    });
                };

                handler.setState = function(state){
                    handler.state = state;
                };

                handler.reset = function(){
                    console.log('delay'+handler.delay);
                        $timeout(function () {
                            handler.setState(0);
                        }, handler.delay);
                };

                handler.getState = function(){
                    return handler.state;
                };

            };


            return {
                getHandler: function(delay){
                    return new Handler(delay);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Middleware', function($rootScope, $timeout, $state, MainConfig, $stateParams){

			var auth = function(isAuth, event, toState){
				if(isAuth){
					if(toState.name === 'auth') {
						event.preventDefault();
						$state.go('home',{},{location:'replace'});
					}
				}else{
					if(toState.name !== 'auth'){
						event.preventDefault();
						$state.go('auth',{},{location:'replace'});
					}
				}
			};



            var fakeIt = function(event,toState){
                //fake the authentication
                $rootScope.authenticated = true;
                $rootScope.user = {name:'Someone',type:0};

                $timeout(function() {
                    console.log('Login');
                    $rootScope.$broadcast('onLoginComplete',{});
                },1500);

                if(!MainConfig.getBackend()){
    				event.preventDefault();
    				MainConfig.resolveConfig(function(success){
    					if(success)

    						$state.go(toState.name);

    				});
    			}else{
    				console.log(MainConfig.backend);
    			}
            };

            return {
				auth : auth,
                fakeIt : fakeIt
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Pagination', function($timeout){

            var Paginator = function(pagefunc, params, success, fail){
                var paginator = this;

                paginator.reset = function(params){
                    paginator.page = 1;
                    paginator.results = [];
                    paginator.details = {};
                    paginator.params = params;
                };
                paginator.isLoading = false;
                paginator.reset(params);

                paginator.loadMore = function(){

                    if(paginator.isLoading){
                        return;
                    }

                    paginator.isLoading = true;
                    paginator.params.page = paginator.page;

                    pagefunc(paginator.params,function(response){
                        paginator.details = response;
                        console.log(paginator.page);
                        paginator.results = paginator.results.concat(response.data);
                        if(response.data.length>0){
                          paginator.page++;
                        }
                        paginator.isLoading = false;
                        console.log(response);
                        success(paginator.results, paginator.details);
                    },function(err){
                        console.log(err);
                        paginator.isLoading = false;
                        fail(err);
                    });
                };
            };
            return {
                getPaginator: function(pagefunc, success, fail){
                    return new Paginator(pagefunc, success, fail);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');

    services.factory('AuthFactory', function($resource) {
      return $resource('./api/', {}, {
          refreshToken: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/refreshtoken'
          },
          password: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/password'
          }
      });
    });

	services.factory('AuthService', function($rootScope,$auth,$state,$http, AuthFactory){
            var isLogggedOut = true;
            var explicitLogOut = false;
            var login = function(credentials, success, fail){

                $auth
                 .login(credentials)
                 .then(function(response){
                        console.log(response);
                        isLogggedOut = false;
                        var data = response.data;
                        $auth.setToken(data.token);
                        $http.defaults.headers.common.Authorization = 'Bearer '+data.token;
                        $rootScope.authenticated = true;
                        $rootScope.user = data.user;
                        $rootScope.$broadcast('onLoginComplete',{});
                        success(data);

                     }, function(error) {
                         console.log(error);
                         $rootScope.authenticated = false;
                         $rootScope.user = null;
                         $auth.removeToken();
                         fail(error);
                 });

            };

            var autoLogin = function(success, fail) {
                    var token = $auth.getToken();
                    $http.defaults.headers.common.Authorization = 'Bearer '+token;
                    AuthFactory.refreshToken(function(res){
                        isLogggedOut = false;
                        var data = res; // we are using ngResource here!
                        $auth.setToken(data.token);
                        $http.defaults.headers.common.Authorization = 'Bearer '+data.token;
                        $rootScope.authenticated = true;
                        $rootScope.user = res.user;
                        $rootScope.$broadcast('onLoginComplete',{});
                        success(data);
                    },function(err){
                        console.log(err);
                        $rootScope.authenticated = false;
                        $rootScope.user = null;
                        $auth.removeToken();
                        fail(false);
                    });
            };

            var logout = function(cb) {
                isLogggedOut = true;
                explicitLogOut = true;
                $http.defaults.headers.common.Authorization = '';
                $rootScope.authenticated = false;
                $rootScope.user = null;
                $auth.removeToken();
                $rootScope.$broadcast('onLogoutComplete',{});
                cb();
            };

            var getExplicitLogOut = function(){
                return explicitLogOut;
            };

            var isLoggedIn = function(){

            };

            return {
                login: login,
                autoLogin : autoLogin,
                logout : logout,
                getExplicitLogOut : getExplicitLogOut
            };
    });
})();
