

var app = angular.module('app', [
  'ngResource',
  'directives'
]);

var baseURL = "./api";

app.factory('BackEnd',
  function ($resource){
      return $resource(baseURL + "/schedule/all", {querytype: '@querytype', id : '@id'},{
          schedule: {method: 'GET', cache: false, isArray: false},
          registerStart: {method:'POST',cache:false,isArray:false,url:baseURL + '/registration/register'},
          PreferenceStat: {method:'POST',cache:false,isArray:false,url:baseURL + '/WorkshopPreference'},
          SendEMail: {method:'POST',cache:false,isArray:false,url:baseURL + '/mail'},
          checkStatus: {method:'POST',cache:false,isArray:true,url:baseURL + '/registration/status/:id'},
          preference : {method:'POST',cache:false,isArray:false,url:baseURL + '/preference'},
          workshops : {method:'GET',cache:false,isArray:true,url:baseURL + '/workshop/all'},
          feedback : {method:'POST', isArray:false, url:baseURL + '/feed/create'}
      });
      
  });

(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ContactController', function ($scope, BackEnd, $window) {
        console.log('contact');
        $scope.contact = {}; //inputs
        $scope.contactLoading = false;
        $scope.contactSend = false;
        $scope.addFeed = function ($event) {
            $scope.submitted = true;
            if(!$scope.contact.message){
                return false;
            }else{
                $scope.contactLoading = true;
                console.log($scope.contact);
                BackEnd.feedback($scope.contact, function (res) {
                    console.log(res);
                    $scope.contactSend = true;
                }, function (err) {
                    console.log(err);
                });
            }
        };


    });

})();

(function() {

    'use strict';

    var app = angular.module('directives', [
    ]);

    app.directive('loader', function(){
        return {
            restrict: 'E',
            scope:{
            },
            link: function(scope, element, attr) {

            },
            template: '<div class="loader"></div>'
        };
    });

    app.directive('radio', function(){
        return {
            restrict: 'E',
            scope:{
                model : '=',
                value : '@',
                label : '@'
            },
            link: function(scope, element, attr) {

                scope.set = function () {
                    if(scope.model==scope.value){
                        scope.model = undefined;
                        return;
                    }
                    scope.model = scope.value;
                };

            },
            templateUrl: './templates/directives/radio.html'
        };
    });

    app.directive('stack', function(){
        return {
            restrict: 'E',
            scope:{
                content : '='
            },
            link: function(scope, element, attr) {
                scope.title = scope.content.title;
                scope.start = scope.content.start;
                scope.end = scope.content.end;
                scope.height = 2;
                scope.color = 'red';
                scope.type='stack';
            },
            templateUrl: './templates/directives/stack.html'
        };
    });




})();

(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('NavController', function ($scope) {
        $scope.isOpen = false;
        $scope.toggle = function () {
                $scope.isOpen = !$scope.isOpen;
                console.log('toggle');
        };
    });

})();

(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('PreferenceController', function ($scope, BackEnd, $window) {
        $scope.in = {}; //inputs
        $scope.isLoading = false;

        BackEnd.workshops(function (res) {
            $scope.workshops = res;
            $scope.fetched = true;
            console.log(res);
        }, function (err) {
            console.log(err);
        });

        $scope.register = function () {
            $scope.isLoading = true;
            console.log($scope.workshops);
            BackEnd.preference($scope.workshops, function (res) {
                $scope.isLoading = false;
                $scope.isDone = true;
                console.log(res);
            }, function (err) {
                console.log(err);
            });
        };

    });

})();

(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('RegController', function ($scope, BackEnd, $window) {
        $scope.in = {}; //inputs
        $scope.isLoading = false;
        $scope.showForm = false;
        $scope.register = function ($event) {
            $scope.submitted = true;
            if( !$scope.in.name ||
                !$scope.in.email ||
                !$scope.in.mobile ||
                !$scope.in.reg_type ||
                !$scope.in.organization_name||
                !$scope.in.gender ||
                !$scope.in.food_preference||
                !$scope.in.accomodation
            ){
                return false;
            }else{
                post('./register',$scope.in,"post");
            }


        };

        $scope.show = function () {
            $scope.showForm = true;
        };

        $scope.$watch('in.reg_type', function(oldV,newV) {
            console.log('watch!!');
            if($scope.in.reg_type=='TYPE_NITC'){
                $scope.in.organization_name = 'NITC';
            }else{
                $scope.in.organization_name = undefined;
            }
        });


        var post = function(path, params, method) {
                method = method || "post";

                var form = document.createElement("form");
                form.setAttribute("method", method);
                form.setAttribute("action", path);
                for(var key in params) {
                    if(params.hasOwnProperty(key)) {
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", key);
                        hiddenField.setAttribute("value", params[key]);
                        form.appendChild(hiddenField);
                     }
                }
                document.body.appendChild(form);
                form.submit();
        };

    });

})();

(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ScheduleController', function($scope, BackEnd){


        $scope.in = {};

        $scope.$watch('in.day', function (newValue, oldValue) {
            console.log(newValue);
            if($scope.in.day)
                sort($scope.in.day);
        });

        var sort = function (index) {
                var slots = $scope.schedule[index].slots;
                var venue = {
                    v1 : [], //ssl
                    v2 : [], //nsl
                    v3 : [] //chanakya
                };
                for (var i = 0; i < slots.length; i++) {
                    var sessions = slots[i].sessions;
                    for (var j = 0; j < sessions.length; j++) {
                        var s = sessions[j];
                        var ses = {
                            //venue : $scope.schedule.rooms[s.room].title,
                            start: s.start,
                            end: s.end,
                            title : s.title
                        };
                        var rand = Math.random();

                        if(s.room=='nitc/software-systems-laboratory-ssl-nitc'){
                            venue.v1.push(s);
                        }else if(s.room=='nitc/network-systems-lab-nsl-nitc'){
                            venue.v2.push(s);
                        }else{
                            venue.v3.push(s);
                        }
                    }
                }
                $scope.schedulestack = venue;
        };


      BackEnd.schedule(function(res){
          console.log(res);
          $scope.schedule = res.schedule;
          $scope.in.day = '0';
          sort(0);
      },function(err){
        console.log(err);
      });
    });

})();
